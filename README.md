# me-busy
---

Angular directive providing a *Google Now* like busy indicator.

* [Demo](http://plnkr.co/edit/2MWVZpiVRu4lAqWhUDBX?p=preview)
* [Example](./src/5b6c525bb8b7d1623328856f1b410338ed1446ad/example/?at=master)

## Development
---

### Prepare your environment

* Install [Node.js](https://nodejs.org/) and NPM
* Install global dev dependencies: ` npm install -g bower gulp `
* Install local dependencies: ` npm install && bower install ` from repository directory

### Development Commands

* ` gulp ` to bundle and watch src files for changes
* ` gulp watch ` to watch src files and bundle / build dist files when changed
* ` gulp bundle ` to combine / minify CSS and jshint, combine, and uglify JS